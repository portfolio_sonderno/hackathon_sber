import psycopg2
from psycopg2 import OperationalError

class Postgre_db:
	def __init__(self, db_name, db_user, db_password, db_host, db_port):
		try:
			self.connection = psycopg2.connect(
				database=db_name,
				user=db_user,
				password=db_password,
				host=db_host,
				port=db_port,
			)
			print("Connection to PostgreSQL DB successful")
		except OperationalError as e:
			print(f"The error '{e}' occurred")

	def execute_query(self, query):
		self.connection.autocommit = True
		cursor = self.connection.cursor()
		try:
			# print(f"Query '{query}' executed successfully")
			cursor.execute(query)
		except OperationalError as e:
			print(f"The error '{e}' occurred")

	def execute_read_query(self, query):
		self.connection.autocommit = True
		cursor = self.connection.cursor()
		try:
			cursor.execute(query)
			result = cursor.fetchall()
			return result

		except OperationalError as e:
			print(f"The error '{e}' occurred")
