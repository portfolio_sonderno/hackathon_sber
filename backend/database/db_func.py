import bcrypt
from database.postgres import Postgre_db 

class Sber_db(Postgre_db):

    def find_username_password(self, username):
      try:
        username_password = self.execute_read_query(f"""
          select name, password_hash from User
          where name = '{username}'
        """)
        if username_password:
          return username_password
      except OSError as e:
        print("Error on find_username_password", e)

    
    def products_all(self, username):
      try:
        events_of_user = self.execute_read_query(f"""
            select "Product".name, 
            "Product".proteint, 
            "Product".carbonhydrate, 
            "Product".fats,
            "Product".energy, 
            "Product".grade, 
            "Allergen"."name", 
            "Additive"."name", 
            "Additive".description,
            "Meal".date
            from "User" join "Meal" on "User".id = "Meal".user_id
            join "Product" on "Meal".id = "Product".meal_id
            join "Allergen" on "Product".id = "Allergen".product_id
            join "Additive" on "Product".id = "Additive".product_id
            where "User".username = '{username}';
        """)
        if events_of_user:
          return events_of_user
      except OSError as e:
        print("Error on login_user", e)

    def search_product_info(self, product_name):
      try:
        product_name_lower = product_name.lower()
        search_product = self.execute_read_query(f'''
            select "Product".name, "Product".proteint, "Product".carbonhydrate, "Product".fats,
            "Product".energy, "Product".grade, "Allergen"."name", "Additive"."name", "Additive".description, "Meal"."date" 
            from "User" join "Meal" on "User".id = "Meal".user_id
            join "Product" on "Meal".id = "Product".meal_id
            join "Allergen" on "Product".id = "Allergen".product_id
            join "Additive" on "Product".id = "Additive".product_id
            where "Product"."name" = '{product_name_lower}';
        ''')
        if search_product:
          return search_product
      except OSError as e:
        print("Error on search_product_info", e)

    def create_user(self, firstname, lastname, username, password, email, tgusername, phonenumber):
      try:
        self.execute_query(f"""
          INSERT INTO users (firstname, lastname, username, password, email, tgusername, phonenumber) VALUES ('{firstname}', '{lastname}', '{username}', '{password}', '{email}', '{tgusername}', '{phonenumber}')
        """)
      except OSError as e:
        print('Error on read_user_id: ', e)

    def all_users(self):
      try:
        all_users = self.execute_read_query(f"""
          SELECT users.id, users.firstname, users.lastname, users.username, users.email from users;
        """)
        if all_users:
          return all_users
      except OSError as e:
        "Error on all_users", e

    def after_create(self, email):
      try:
        first_last = self.execute_read_query(f"""
          SELECT users.id, users.firstname, users.lastname, users.email FROM users
          WHERE users.email = '{email}';
        """)
        if first_last:
          return first_last
      except OSError as e:
        print("Error on after_create", e)

