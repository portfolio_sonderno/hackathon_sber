from crypt import methods
from email.policy import default
from encodings import utf_8
from encodings.utf_8 import encode
from http.client import responses
import json
import string
from urllib import response
import datetime
import bcrypt
from flask import Flask, render_template, request, url_for, redirect
from flask_cors import CORS
from database import db


def create_app():

    def serialisation_all(res):
        def helper(res):
                return { 
                'product_name'  : res[0],
                'protein'       : res[1],
                'carbonhydrate' : res[2],
                'fats'          : res[3],
                'energy'        : res[4],
                'grade'         : res[5],
                'allergen_name' : res[6],
                'additive_name' : res[7],
                'additive_desc' : res[8],
                'date'          : res[9]
            }
        pre_result = list(map(helper, res))
        new_obj = {}

        
        for obj in pre_result:
            print(new_obj.get(obj['product_name']))
            if not new_obj.get(obj['product_name']): new_obj[obj['product_name']] = {}
            link = new_obj[obj['product_name']]
            print(link)

            # print(obj.items())
            for hhe in list(obj.items())[1:]:
                print(hhe)
                if not link.get(hhe[0]): link[hhe[0]] = hhe[1]
                if type(link[hhe[0]]) == str and link[hhe[0]] != hhe[1]: 
                    old = link[hhe[0]]
                    link[hhe[0]] = [old, hhe[1]]
                if type(link[hhe[0]]) == list: 
                    if hhe[1] in link[hhe[0]]: continue
                    link[hhe[0]].append(hhe[1])
        return new_obj

    def serialisation_one(res):
        def helper(res):
                return { 
                'product_name'  : res[0][0],
                'protein'       : res[0][1],
                'carbonhydrate' : res[0][2],
                'fats'          : res[0][3],
                'energy'        : res[0][4],
                'grade'         : res[0][5],
                'allergen_name' : res[0][6],
                'additive_name' : res[0][7],
                'additive_desc' : res[0][8],
                'date'          : res[0][9]
            }
        pre_result = list(map(helper, res))
        new_obj = {}

        
        for obj in pre_result:
            print(new_obj.get(obj['product_name']))
            if not new_obj.get(obj['product_name']): new_obj[obj['product_name']] = {}
            link = new_obj[obj['product_name']]
            print(link)

            # print(obj.items())
            for hhe in list(obj.items())[1:]:
                print(hhe)
                if not link.get(hhe[0]): link[hhe[0]] = hhe[1]
                if type(link[hhe[0]]) == str and link[hhe[0]] != hhe[1]: 
                    old = link[hhe[0]]
                    link[hhe[0]] = [old, hhe[1]]
                if type(link[hhe[0]]) == list: 
                    if hhe[1] in link[hhe[0]]: continue
                    link[hhe[0]].append(hhe[1])
        return new_obj


    app = Flask(__name__)
    cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
    app.config['JSON_AS_ASCII'] = False
    def myconverter(o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    @app.route('/api/signup', methods=['POST'])
    def sign_up_user():
        content_type = request.headers.get('Content-Type')
        if (content_type == 'application/json'):
            firstname = request.json['firstName']
            print(firstname)
            lastname = request.json.get("lastName", None)
            username = request.json.get("userName", None)
            hashed_password = bcrypt.hashpw(request.json.get("password", None), bcrypt.gensalt(10))
            email = request.json.get("email", None)
            tgusername = request.json.get("tgUsername", None)
            phonenumber = request.json.get("phone", None)
            if db.after_create(email):
                return "Пользователь с таким email существует", 401
            db.create_user(firstname, lastname, username, hashed_password, email, tgusername, phonenumber)
            return json.dumps(db.after_create(email)[0]).encode('utf8'), 201
        else:
            return 'Content-Type not supported!'

    @app.route('/api/login', methods=['POST'])
    def login_user():
        content_type = request.headers.get('Content-Type')
        if (content_type == 'application/json'):
            email = request.json.get('email', None)
            password = request.json.get('password', None)
            checking_password = db.find_email_users(email)[0][0]
            if bcrypt.hashpw(password, checking_password) == checking_password:
                all_users = list(map(lambda o: {
                    'id': o[0],
                    'first_name': o[1],
                    'last_name': o[2],
                    'username': o[3],
                }, db.all_users()))
                
                res = db.login_user(email)
                obj = {
                    'id': res[0][0],
                    'first_name': res[0][1],
                    'last_name': res[0][2],
                    'username': res[0][3],
                    'events': []
                }
                for r in res:
                    obj['events'].append({
                        'eventName': r[4],
                        'eventStart': r[5],
                        'eventEnd': r[6],
                        })
                print(obj)
                return [obj, all_users], 201
            else:
                    return "Пароль неправильный", 401

    @app.route('/api/new_event', methods=['POST'])
    def new_event():
        content_type = request.headers.get('Content-Type')

    @app.route('/api/product_properties', methods=['POST'])
    def return_product_prop():
        content_type = request.headers.get('Content-Type')
        if (content_type == 'application/json'):
            username = request.json.get('username', None)
            res = db.products_all(username)
            new_obj = serialisation_all(res)
                    
            return new_obj, 201
        else:
            return 'Content-Type not supported!', 404
        
    @app.route('/api/search_product', methods=['POST'])
    def search_product():
        content_type = request.headers.get('Content-Type')
        if (content_type == 'application/json'):
            product_name = request.json
            new_list = []
            for i in product_name:
                new_list.append(db.search_product_info(i))
            #res = db.search_product_info(new_list)
            new_new_list = []
            for i in new_list:
                new_new_list.append(serialisation_all(i))
            #new_obj = serialisation_all(new_list)
            obj = {}
            for i, val in enumerate(new_new_list):
                obj[i] = val
            return obj, 201
        else:
            return 'Content-Type not supported!', 404



    return app


def main():
    app = create_app()

if __name__ == '__main__':
    main()